#!/usr/bin/env bash

echo "Running..."

cd actors

npm run exec
if [ $? -ne 0 ]; then
   echo "RUN FAILED"
   exit 1
fi
