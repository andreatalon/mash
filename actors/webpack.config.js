const path = require('path')

module.exports = {
  mode: 'development',
  target: 'node',
  entry: {
    server: './src/server.js'
  },
  output: {
    library: 'src',
    libraryTarget: 'commonjs2',
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  node: {
    Buffer: false,
    fs: 'empty'
  },
  module: {
    noParse: /lodash\/lodash.js/,
    rules: [
      {test: /\.js?$/, exclude: /node_modules/, use: ['babel-loader', {loader: 'eslint-loader', options: {include: path.resolve(__dirname, 'src'), enforce: 'pre', failOnError: true}}]}, // babel-loader must be specified before eslint-loader
    ]
  },
  externals: {
    winston: 'winston'
  }
}
