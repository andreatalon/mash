import 'regenerator-runtime/runtime'

import _ from 'lodash'
import http from 'http'
import uuidv4 from 'uuid/v4'
import WebSocket from 'websocket'

import {prepare} from './workspace'

const rand = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)

const exec = async() => {
  const logger = prepare()

  const s = http.createServer((req, res) => {
    //
  })

  s.listen(1337, () => {})

  const ws = new WebSocket.server({
    httpServer: s
  })

  ws.on('request', request => {
    const connection = request.accept(null, request.origin)

    connection.on('connection', connection => {
      // logger.debug(connection)
    })

    connection.on('message', message => {
      // logger.debug(message)
    })

    connection.on('close', connection => {
      // logger.debug(connection)
    })
  })

  console.log('http://localhost:8765')

  let i = 0

  i = 1

  while (i) {
    const n = uuidv4()

    // console.log(n)
    // logger.debug(n)

    ws.connections.forEach(connection => {
      connection.send(JSON.stringify({type: 'notify', value: `notification: ${n}`}))
    })

    await new Promise(resolve => setTimeout(resolve, rand(1000, 10000)))
  }
}

export {
  exec
}
