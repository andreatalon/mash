import 'regenerator-runtime/runtime'

import {prepare} from './workspace'
import {exec as beExec} from './be'
import {exec as feExec} from './fe'
import {exec as wsExec} from './ws'

const exec = async() => {
  const logger = prepare()

  console.log('Here I am!')
  logger.debug('Here I am!')

  beExec()
  feExec()
  wsExec()
}

export {
  exec
}
