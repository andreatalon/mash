import 'regenerator-runtime/runtime'

import _ from 'lodash'
// import axios from 'axios'
import cors from 'cors'
import express from 'express'
import fetch from 'node-fetch'
import {MongoClient} from 'mongodb'
// import mongoose, {Schema, Model} from 'mongoose'
import saslprep from 'saslprep'
import Unsplash, {toJson} from 'unsplash-js'

import misc from './misc'
import {prepare} from './workspace'

global.fetch = fetch

const exec = async() => {
  const logger = prepare()

  const accessKey = 'dRGcpzEjIcVHiIzWESwZc3uwZFrN9g_Yj4ovRr07Uvw'
  const unsplash = new Unsplash({accessKey})

  const dbUri = 'mongodb+srv://admin:admin@h-mash.dpykf.mongodb.net/h-mash?retryWrites=true&w=majority'
  let mash

  // mongoose
  //   .connect(dbUri, {
  //     useNewUrlParser: true,
  //     useUnifiedTopology: true
  //   })
  //   .then(client => {
  //     console.log('MongoDB Connected...')

  //     mash = client.db('h-mash').collection('h-mash')
  //   })
  //   .catch(err => console.log(err))

  // const mc = mongoose.connection

  MongoClient
    .connect(dbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(client => {
      console.log('MongoDB Connected...')

      mash = client.db('h-mash').collection('h-mash')
    })
    .catch(err => console.log(err))

  const be = express()

  be.use(cors())
  be.use(express.json())

  be.get('/', (req, res) => {
    res.send('Hello, /')
  })

  be.get('/search', (req, res) => {
    const q = req.query

    // axios.get(`/search/photos?page=${q.page}&per_page=${q.per_page}&query=/${q.query}`)
    //   .then(aRes => {
    //     res.send(aRes.data)
    //   })

    unsplash.search
      .photos(q.keyword, q.page, q.per_page)
      .then(toJson)
      .then(json => res.send(json))
  })

  be.get('/detail', (req, res) => {
    const q = req.query

    unsplash.photos
      .getPhoto(q.id)
      .then(toJson)
      .then(json => res.send(json))
  })

  be.get('/mash', async(req, res) => {
    const q = req.query
    let d = await mash.findOne({ref: q.ref})

    if (misc.empty(d)) {
      res.send(null)

      return
    }

    d = toJson(d)

    res.send(d)
  })

  be.post('/record', async(req, res) => {
    const b = req.body
    let d = await mash.findOne({ref: b.ref})

    if (misc.empty(d) && !misc.empty(b.saving)) {
      // insert
      await mash.insertOne({ref: b.ref, himages: b.himages})
    } else {
      d = toJson(d)

      if (b.saving) {
        // update
        d.himages = _.uniqBy(misc.empty(d.himages) ? b.himages : d.himages.concat(b.himages), 'id')
      } else if (d) {
        // delete
        d.himages = _.remove(d.himages, dHimage => _.findIndex(b.himages, bHimage => dHimage.id === bHimage.id) < 0)
      }

      await mash.findOneAndUpdate({ref: b.ref}, {$set: d})
    }

    res.send()
  })

  be.listen(3000)
}

export {
  exec
}
