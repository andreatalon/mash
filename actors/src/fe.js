import 'regenerator-runtime/runtime'

import _ from 'lodash'
import connect from 'connect'
import historyApiFallback from 'connect-history-api-fallback'
import gzipStatic from 'connect-gzip-static'
import log4js from 'log4js'
import morgan from 'morgan'
import serveStatic from 'serve-static'
import {prepare} from './workspace'

const exec = async() => {
  const logger = prepare()

  const fe = connect()

  fe.use(morgan('composed', {
    stream: {
      write: log => {
        (log4js.getLogger()).debug(log)
      }
    }
  }))

  fe.use(function(req, res, next) {
    if (req._parsedUrl.pathname.match(/healthcheck/)) {
      res.writeHead(200)
      res.end('I\'m fine!')

      return
    }
    next()
  })

  const staticApp = '../static/public'

  fe.use(serveStatic(staticApp))
  fe.use(historyApiFallback())
  fe.use(gzipStatic(staticApp, {maxAge: 86400000, override: true}))

  fe.use(function(req, res, next) {
    if (req._parsedUrl.pathname.match(/index\.(js|css)$/)) {
      req.url += '.gz'
      res.setHeader('Content-Encoding', 'gzip')
    } else {
      req.url = '/'
    }
    next()
  })

  fe.use('/foobar', function(req, res) {
    res.end('foobar!\n')
  })

  fe.listen(80)
}

export {
  exec
}
