import fs from 'fs'
import path from 'path'
import winston from 'winston'

const prepare = () => {
  if (!fs.existsSync(path.resolve('.', 'log'))) {
    fs.mkdirSync(path.resolve('.', 'log'))
  }

  winston.configure({
    level: 'debug',
    format: winston.format.simple(), // .json()
    transports: [
      new winston.transports.File({filename: path.resolve('.', 'log/node.log')})
    ]
  })

  return winston
}

export {
  prepare
}
