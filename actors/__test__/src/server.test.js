'use strict'

import 'regenerator-runtime/runtime'

describe('handler', () => {
  test('jest is working', () => {
    expect(true).not.toBe(false);

    const x = 2 + 2

    expect(x).toBe(4);
    expect(x).toBeDefined();
  })
})
