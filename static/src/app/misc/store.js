import {createStore, combineReducers, applyMiddleware, compose} from 'redux'
import {connectRouter, routerMiddleware} from 'connected-react-router'
import {combineEpics, createEpicMiddleware} from 'redux-observable'
import * as epics from '../actions/epics'
import history from './history'
import himages from '../reducers/himages'
import mash from '../reducers/mash'
import workplace from '../reducers/workplace'

const epicMiddleware = createEpicMiddleware()

const store = createStore(
  combineReducers({
    himages,
    mash,
    workplace,
    router: connectRouter(history)
  }),
  undefined, // initialState,
  compose(
    applyMiddleware(
      routerMiddleware(history),
      epicMiddleware
      // createEpicMiddleware(combineEpics(...Object.values(epics)))
      // ... other middlewares ...
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
)

epicMiddleware.run(combineEpics(...Object.values(epics)))

export default store
