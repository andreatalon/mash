import auth0 from 'auth0-js'
import config from './config'

const auth = new auth0.WebAuth({
  audience: `https://${config.auth0.DOMAIN}/userinfo`,
  clientID: config.auth0.CLIENT_ID,
  domain: config.auth0.DOMAIN,
  leeway: 60,
  redirectUri: config.auth0.CALLBACK_URL,
  responseType: 'token id_token',
  returnTo: config.auth0.LOGOUT_URL,
  sameSite: 'lax',
  scope: 'openid'
})

export default auth
