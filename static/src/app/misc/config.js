import misc from './misc'

const config = {
  actionTimeout: 3000,

  auth0: {
    DOMAIN: 'dev-qr4b93uj.eu.auth0.com',
    CLIENT_ID: 'R1rxGD3ZYSkQZYgWQVFFLXiD6R4SrX7A',
    CALLBACK_URL: `${window.location.origin}/callback`,
    LOGOUT_URL: `${window.location.origin}/logout`
  },

  authorizedHeaders() {
    return Object.assign(this.headers(), {
      'Authorization': 'Bearer ' + localStorage.getItem('idToken')
    })
  },

  hasAuthorizedHeaders() {
    return !misc.empty(localStorage.getItem('idToken'))
  },

  headers() {
    return {
      'Content-Type': 'application/json'
    }
  },

  service(which, uri) {
    return this.url()[which] + uri
  },

  url() {
    switch (window.location.port) {
    case '8765':
    case '8767':
      return {
        be: 'http://localhost:8764',
        ws: 'ws://localhost:8766'
      }
    default:
      return {
        be: '',
        ws: ''
      }
    }
  }
}

export default config
