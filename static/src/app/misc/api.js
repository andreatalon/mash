const api = {
  GET_ENV: '/env',

  POST_LOGIN: 'https://dev-qr4b93uj.eu.auth0.com/api/v2/token', // '/auth/login',
  // POST_REFRESH: '/auth/refresh',
  // POST_LOGOUT: '/auth/logout',

  GET_SEARCH: '/search?keyword=:keyword&page=:page&per_page=:per_page',
  GET_DETAIL: '/detail?id=:id',
  GET_MASH: '/mash?ref=:ref',
  POST_RECORD: '/record'
}

export default api
