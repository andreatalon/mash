import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import misc from '../misc/misc'

class FlashMessage extends Component {
  render() {
    if (this.props.flashedMessage === null) {
      return null
    }

    const messageType = misc.empty(this.props.messageType) ? misc.messageType.INFO : this.props.messageType

    return (
      <div key={`flashed-message-${misc.hash()}`} className='flash-message'>
        <strong>{messageType.icon}</strong> {this.props.flashedMessage}
      </div>
    )
  }
}

FlashMessage.propTypes = {
  flashedMessage: PropTypes.string,
  messageType: PropTypes.object
}

function mapStateToProps(state) {
  return {
    flashedMessage: state.workplace.flashedMessage,
    messageType: state.workplace.messageType
  }
}

export default connect(
  mapStateToProps
)(FlashMessage)
