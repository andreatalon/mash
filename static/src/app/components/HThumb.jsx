import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Card, Dimmer, Image, Loader} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import _ from 'lodash'
import history from '../misc/history'
import misc from '../misc/misc'

class HThumb extends Component {
  constructor(props) {
    super(props)

    this.state = {
      height: 150,
      marginTop: 5,
      marginBottom: 15
    }

    this.handleGotoDetail = this.handleGotoDetail.bind(this)
  }

  toRender() {
    const {
      hi,
      himagesLimit,
      scrollableProps
    } = this.props

    const {
      height,
      marginTop,
      marginBottom
    } = this.state

    if (misc.empty(scrollableProps.scrollTop)) {
      return true
    }

    if (scrollableProps.scrollTop < 1 && hi < 50) {
      return true
    }

    const h = height + marginTop + marginBottom

    if (scrollableProps.scrollTop > 0) {
      const cols = Math.ceil(himagesLimit * h / scrollableProps.scrollHeight)
      // const rows = Math.floor(himagesLimit / cols)
      const myRow = Math.floor(hi / cols)
      const top = myRow * h
      const bottom = top + h

      if (
        (top >= scrollableProps.scrollTop && top <= (scrollableProps.scrollTop + scrollableProps.offsetHeight)) ||
        bottom >= scrollableProps.scrollTop && bottom <= (scrollableProps.scrollTop + scrollableProps.offsetHeight)) {
        return true
      }
    }

    return false
  }

  handleGotoDetail(e) {
    try {
      if (!window.getSelection().getRangeAt(0).collapsed) {
        e.preventDefault()
        e.stopPropagation()

        return false
      }
    } catch (e) {
      console.log(e)
    }

    // this.props.storeHImage(this.props.himage)

    this.props.updateHImagesState({
      himagesSelected: [this.props.himage],
      selectedAll: false
    })

    history.push(`/detail/${this.props.himage.id}`)

    e.stopPropagation()
  }

  render() {
    const {
      activeCalls,
      handleSelect,
      selected,
      himage
    } = this.props

    const noRender = !this.toRender()
    const loading = (selected && activeCalls.indexOf('actionHImagesEpic') > -1) || activeCalls.indexOf(himage.uid) > -1

    return (
      <Card className={'himage-card' + (selected ? ' himage-card-selected' : '') + (noRender ? ' himage-card-fake' : '')} himage={himage} onClick={handleSelect} style={{height: `${this.state.height}px`, marginTop: `${this.state.marginTop}px`, marginBottom: `${this.state.marginBottom}px`}}>
        {noRender ? null : <Card.Content>
          <Dimmer.Dimmable dimmed={loading}>
            <div className='card-content' style={{height: `${this.state.height}px`}}>
              <div className='himage-preview-image'>
                <Image async onError={e => {e.target.error = null; e.target.style.visibility = 'hidden'}} src={_.get(himage, 'urls.thumb')} onClick={this.handleGotoDetail}/>
              </div>
              <Button className='himage-select' content={`${selected ? 'de' : ''}select`} icon={selected ? 'close' : 'checkmark'}/>
            </div>
            <Dimmer inverted active={loading} style={{height: `${this.state.height}px`}}>
              <Loader active/>
            </Dimmer>
          </Dimmer.Dimmable>
        </Card.Content>}
      </Card>
    )
  }
}

HThumb.propTypes = {
  activeCalls: PropTypes.array,
  handleSelect: PropTypes.func,
  scrollableProps: PropTypes.object,
  selected: PropTypes.bool,
  hi: PropTypes.number,
  himage: PropTypes.object,
  himagesLimit: PropTypes.number
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls
  }
}

function mapDispatchToProps(dispatch) {
  return {
    storeHImage: bindActionCreators(actions.storeHImage, dispatch),
    updateHImagesState: bindActionCreators(actions.updateHImagesState, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HThumb)
