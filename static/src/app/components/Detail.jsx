import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Dimmer, Grid, Icon, Image, Loader, Message, Step, Table} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import ActionButtons from './ActionButtons'
import _ from 'lodash'
import history from '../misc/history'
import misc from '../misc/misc'

class Detail extends Component {
  constructor(props, context) {
    super(props, context)

    window.scrollTo(0, 0)

    this.state = {
      recovering: true
    }
  }

  componentWillUnmount() {
    this.props.updateHImagesState({
      himagesSelected: []
    })
  }

  render() {
    const {
      activeCalls,
      callHImage,
      himages
    } = this.props

    const id = location.pathname.split('/detail/')[1]

    if (misc.empty(id)) {
      history.replace('/404')

      return null
    }

    const himage = himages.find(himage => himage.id === id)

    if (misc.empty(himage)) {
      if (this.state.recovering) {
        this.setState({
          recovering: false
        })

        callHImage(id)
      }

      if (activeCalls.indexOf('callHImageEpic') > -1) {
        return (
          <Grid doubling centered>
            <Grid.Row>
              <Loader inline active/>
            </Grid.Row>
          </Grid>
        )
      }

      return (
        <Message icon color={misc.messageType.ERROR.color}>
          <Icon name={misc.messageType.ERROR.icon}/>
          <Message.Content>HImage not found!</Message.Content>
        </Message>
      )
    }

    const loading = activeCalls.indexOf('actionHImageEpic') > -1

    const himageSpecifics = {}

    himageSpecifics.user = `${himage.user.first_name} ${himage.user.last_name}`
    himageSpecifics.created = himage.created_at
    himageSpecifics.likes = himage.likes

    return (
      <Grid stackable className='himage' columns={3}>
        <Grid.Row>
          <Grid.Column width={8} className='tc'>
            <div className='himage-preview'>
              <div className='himage-preview-image'>
                <Image async onError={e => {e.target.error = null; e.target.style.visibility = 'hidden'}} src={_.get(himage, 'urls.regular')}/>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width={5}>
            <Dimmer.Dimmable dimmed={loading}>
              <div>
                <h3>{_.get(himage, 'id')}</h3>
                {/* <Table basic='very'>
                  <Table.Body>
                    {misc.empty(himage.status) ? null : <Table.Row>
                      <Table.Cell>ID:</Table.Cell>
                      <Table.Cell><span style={{textTransform: 'uppercase', fontStyle: 'italic'}}>{himage.id}</span></Table.Cell>
                    </Table.Row>}
                  </Table.Body>
                </Table> */}
                <Table celled striped compact>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell colSpan='2'>Details</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {Object.keys(himageSpecifics).map(himageSpecificKey => (misc.empty(himageSpecifics[himageSpecificKey]) ? null : <Table.Row key={`himage-specific-${himageSpecificKey.replace(/ +/g, '-')}`}>
                      <Table.Cell><strong>{himageSpecificKey}</strong></Table.Cell>
                      <Table.Cell>{himageSpecifics[himageSpecificKey]}</Table.Cell>
                    </Table.Row>))}
                  </Table.Body>
                </Table>
              </div>
              <Dimmer inverted active={loading}>
                <Loader active/>
              </Dimmer>
            </Dimmer.Dimmable>
          </Grid.Column>
          <Grid.Column width={3}>
            <ActionButtons key='action-buttons-himage' himagesSelected={[himage]}/>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

Detail.propTypes = {
  activeCalls: PropTypes.array,
  parentProps: PropTypes.object,
  himages: PropTypes.array,
  himagesSelected: PropTypes.array
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls,
    himages: state.himages.himages,
    himagesSelected: state.himages.himagesSelected
  }
}

function mapDispatchToProps(dispatch) {
  return {
    callHImage: bindActionCreators(actions.callHImage, dispatch),
    updateHImagesState: bindActionCreators(actions.updateHImagesState, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detail)
