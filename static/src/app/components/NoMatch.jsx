import React from 'react'

const NoMatch = () => <h1 className='no-match'>:( Page not found</h1>

export default NoMatch
