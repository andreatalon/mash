import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Divider, Form, Header, Icon, Input, Modal, Radio, TextArea} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'

class ActionButtons extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
    }

    this.handleButtonClick = this.handleButtonClick.bind(this)
  }

  handleButtonClick(e, {action}) {
    if (!misc.empty(this.props.himagesSelected)) {
      let a = 'call'
      let saving = true

      switch (action) {
      case 'record': a = 'record'; saving = true; break
      case 'delete': a = 'record'; saving = false; break
      default:
      }

      this.props[`${a}HImages`](misc.ca(), this.props.himagesSelected, saving)

      return
    }
  }

  render() {
    const {
      activeButtons,
      activeCalls,
      compact,
      layout,
      mash
    } = this.props

    let {
      himagesSelected
    } = this.props

    himagesSelected = _.compact(himagesSelected)

    const horizontal = layout === 'horizontal'

    let activeButtonsExist, visibleProps, disabled

    return [
      {name: 'Record', action: 'record', icon: 'save'},
      {name: 'Delete', action: 'delete', icon: 'delete'}
    ].map(action => {
      activeButtonsExist = !misc.empty(activeButtons)

      if (activeButtonsExist) {
        if (this.props.activeButtons.indexOf(action.action) === -1) {
          return null
        }
      }

      visibleProps = {}
      disabled = true

      if (!misc.empty(himagesSelected)) {
        switch (action.action) {
        case 'record': disabled = himagesSelected.length === 0 || window.location.pathname === '/collection'; break // _.filter(himagesSelected, himageSelected => _.findIndex(mash, ['id', himageSelected.id])) === himagesSelected.length
        case 'delete': disabled = himagesSelected.length === 0 || window.location.pathname !== '/collection'; break // _.filter(himagesSelected, himageSelected => _.findIndex(mash, ['id', himageSelected.id])) === 0
        default:
        }
      }

      if (compact) {
        visibleProps = {content: '', icon: action.icon}
      } else {
        visibleProps = {content: <label>{action.name}</label>}
        if (action.icon) {
          visibleProps.icon = action.icon
        }
      }

      const loading = activeCalls.indexOf(`actionHImages${misc.capitalize(action.action)}Epic`) > -1

      const b = [<Button key={'action-' + action.action} loading={loading} disabled={disabled || loading} className={`action-button ${(horizontal ? 'horizontal' : 'vertical')}`} color={action.color} fluid={!(horizontal || activeButtonsExist)} action={action.action} onClick={this.handleButtonClick} basic={activeButtonsExist} compact={activeButtonsExist} {...visibleProps}/>]

      return b
    })
  }
}

ActionButtons.propTypes = {
  activeButtons: PropTypes.array,
  activeCalls: PropTypes.array,
  compact: PropTypes.bool,
  layout: PropTypes.string,
  himages: PropTypes.array,
  himagesSelected: PropTypes.array,
  mash: PropTypes.array
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls,
    himages: state.himages.himages,
    himagesSelected: state.himages.himagesSelected,
    mash: state.mash.mash
  }
}

function mapDispatchToProps(dispatch) {
  return {
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch),
    waiting: bindActionCreators(actions.waiting, dispatch),
    recordHImages: bindActionCreators(actions.recordHImages, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActionButtons)
