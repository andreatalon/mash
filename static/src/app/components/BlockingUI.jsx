import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

class BlockingUI extends Component {
  render() {
    return (
      <div className={`app-blocking ${this.props.blockingCalls.length > 0 ? ' active' : ''}`}/>
    )
  }
}

BlockingUI.propTypes = {
  blockingCalls: PropTypes.array.isRequired
}

function mapStateToProps(state) {
  return {
    blockingCalls: state.workplace.blockingCalls
  }
}

export default connect(
  mapStateToProps
)(BlockingUI)
