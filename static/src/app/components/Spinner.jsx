import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Loader} from 'semantic-ui-react'

class Spinner extends Component {
  render() {
    return <Loader active={this.props.activeCalls.length > 0}/>
  }
}

Spinner.propTypes = {
  activeCalls: PropTypes.array.isRequired
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls
  }
}

export default connect(
  mapStateToProps
)(Spinner)
