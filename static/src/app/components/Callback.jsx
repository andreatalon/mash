import React from 'react'

const Callback = props => {
  if (/access_token|id_token|error/.test(props.location.hash)) {
    props.parentProps.handleAuthentication()
  }

  return <div>logging...</div>
}

export default Callback
