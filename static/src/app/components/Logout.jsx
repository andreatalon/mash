import React from 'react'
import history from '../misc/history'

const Logout = props => {
  localStorage.clear()
  props.parentProps.updateAccessToken('')
  history.push('/home')
  window.location.reload()

  return <div>logging out...</div>
}

export default Logout
