import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {actions} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'
import config from '../misc/config'

class WS extends Component {
  componentDidMount() {
    this.state = this.initState()
  }

  initState() {
    const connection = new WebSocket(config.url().ws)

    let heartbeat = null

    connection.onopen = () => {
      this.props.flashMessageAndReset('Connected! Receiving notifications...', misc.messageType.SUCCESS)

      heartbeat = setInterval(() => connection.send('keep me alive'), 5000)
    }

    connection.onerror = err => {
      this.props.flashMessageAndReset('Connection error!', misc.messageType.ERROR)
    }

    connection.onmessage = event => {
      const data = JSON.parse(event.data)

      switch (data.type) {
      case 'test':
        this.props.flashMessageAndReset(data.value, misc.messageType.SUCCESS)

        break
      default:
      }
    }

    return {
      heartbeat,
      connection
    }
  }

  disconnect() {
    clearInterval(this.state.heartBeat)
    this.state.connection.close()
  }

  render() {
    return null
  }
}

function mapDispatchToProps(dispatch) {
  return {
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch)
  }
}

export default connect(
  null,
  mapDispatchToProps
)(WS)
