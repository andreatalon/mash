import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Route} from 'react-router-dom'
import {actions} from '../actions/actions'
import FlashMessage from './FlashMessage'
import Topbar from './Topbar'
import WS from './WS'
import _ from 'lodash'
import history from '../misc/history'
import misc from '../misc/misc'
import config from '../misc/config'
import auth from '../misc/auth'

class Workplace extends Component {
  componentDidMount() {
    this.updateAccessToken()
    this.checkToken()
  }

  componentDidUpdate() {
    this.checkToken()
  }

  checkToken() {
    if (!(window.location.pathname === '/' || window.location.pathname !== '/home')) {
      return this.enter()
    }

    if (misc.empty(this.props.accessToken)) {
      return this.exit()
    }

    // if (new Date().getTime() > JSON.parse(localStorage.getItem('expiresAt'))) { // expiresAt different from claim 'exp'
    //   return this.exit()
    // }

    if (misc.empty(misc.ca())) {
      return this.exit()
    }

    return this.enter()
  }

  exit() {
    localStorage.clear()
    this.props.updateAccessToken('')

    if (window.location.pathname !== '/login') {
      history.push('/login')
    }
  }

  enter() {
    if (window.location.pathname === '/login') {
      history.push('/')
    }
  }

  updateAccessToken(accessToken) {
    let at = ''

    if (!misc.undef(accessToken)) {
      at = accessToken
    } else if (localStorage.getItem('accessToken')) {
      at = localStorage.getItem('accessToken')
    }

    this.props.updateAccessToken(at)
  }

  handleAuthentication() {
    auth.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        localStorage.setItem('accessToken', authResult.accessToken)
        localStorage.setItem('idToken', authResult.idToken)
        this.updateAccessToken(authResult.accessToken)
      } else if (err) {
        this.updateAccessToken('')
      }
    })
  }

  manageResponse() {
    if (misc.empty(this.props.callResponse)) {
      return
    }

    let r = this.props.callResponse

    if (!misc.empty(r)) {
      if (r.response) {
        r = r.response
      }

      const rc = typeof r.code === 'number' ? `${r.code}: ` : ''
      const rm = misc.undef(r.message) ? '' : r.message
      let message = ''

      if (typeof rm === 'string') {
        message = rm
      } else if (typeof rm === 'object') {
        message = JSON.stringify(rm).replace(/[\{\}\(\)\[\]\"]/g, '')
      }

      this.props.flashMessageAndReset(`${rc} ${message}`, misc.messageTypeByCode(r.code))

      if (message.match(/(invalid token|expired token)/i)) {
        if (config.PROD) {
          auth.logout({federated: true})
        } else {
          this.exit()
        }
      }
    }
  }

  render() {
    const parentProps = {
      ...this.props,
      auth,
      handleAuthentication: this.handleAuthentication
    }

    this.manageResponse()

    return (
      <Route render={props => (
        <div className='workplace'>
          <Topbar parentProps={parentProps}/>
          <FlashMessage/>
          <div className='body'>
            <this.props.component {...props} parentProps={parentProps}/>
          </div>
        </div>
      )}/>
    )
  }
}

Workplace.propTypes = {
  component: PropTypes.func,
  waiting: PropTypes.func,
  refresh: PropTypes.func,
  updateAccessToken: PropTypes.func,
  accessToken: PropTypes.string,
  message: PropTypes.string,
  flashMessage: PropTypes.func
}

function mapStateToProps(state) {
  return {
    accessToken: state.workplace.accessToken,
    message: state.workplace.message,
    callResponse: state.workplace.callResponse
  }
}

function mapDispatchToProps(dispatch) {
  return {
    waiting: bindActionCreators(actions.waiting, dispatch),
    refresh: bindActionCreators(actions.refresh, dispatch),
    updateAccessToken: bindActionCreators(actions.updateAccessToken, dispatch),
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Workplace)
