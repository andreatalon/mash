import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {Button, Grid, Input, Icon, Loader, Menu} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import ActionButtons from './ActionButtons'
import HThumb from './HThumb'
import _ from 'lodash'
import misc from '../misc/misc'

class Home extends Component {
  constructor(props, context) {
    super(props, context)

    this.state = {
      searchedQuery: ''
    }

    window.scrollTo(0, 0)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleSelectAll = this.handleSelectAll.bind(this)
    this.handleDeselectAll = this.handleDeselectAll.bind(this)
    this.handleInterfaceChange = this.handleInterfaceChange.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleSearchedQuery = this.handleSearchedQuery.bind(this)
    this.handleSearchKeyPress = this.handleSearchKeyPress.bind(this)
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleInterfaceChange)
  }

  componentDidMount() {
    this.props.updateHImagesState({
      himagesSelected: [],
      selectedAll: false
    })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleInterfaceChange)
  }

  _recalcVisibleHImages() {
    const {
      himages,
      himagesTotal,
      page,
      per_page,
      sOffsetHeight,
      sScrollHeight,
      sScrollTop
    } = this.props

    const w = this._wrapper

    if (sOffsetHeight !== w.offsetHeight || sScrollHeight !== w.scrollHeight || sScrollTop !== w.scrollTop) {
      this.props.updateHImagesState({
        sOffsetHeight: w.offsetHeight,
        sScrollHeight: w.scrollHeight,
        sScrollTop: w.scrollTop
      })

      if ((w.scrollTop + w.offsetHeight) === w.scrollHeight && himages.length < himagesTotal) {
        this.props.callHImages(this.state.searchedQuery, page + 1, per_page)
      }
    }
  }

  handleSearchedQuery(e, {value}) {
    this.setState({
      searchedQuery: value.replace(/\'/g, '-')
    })
  }

  handleSearch() {
    let searchedQuery = this.state.searchedQuery

    if (misc.empty(searchedQuery)) {
      this.props.flashMessageAndReset('No text, no special chars, 4 minimum', misc.messageType.WARNING)

      return
    }

    if (!searchedQuery.match(/[^()><\]\[\\\x22"',;|%#]{4,}/)) {
      this.props.flashMessageAndReset('No special chars, 4 minimum', misc.messageType.WARNING)

      return
    }

    if (searchedQuery.match(/^[0-9]{1,11}\/[0-9]{1,3}$/)) {
      searchedQuery = searchedQuery.replace(/\//g, '-')

      this.setState({
        searchedQuery
      })
    }

    this.props.callHImages(searchedQuery, 0, this.props.per_page)
  }

  handleSearchKeyPress(e) {
    if (e.charCode === 13) {
      this.handleSearch()
    }
  }

  handleSelect(e, {himage}) {
    try {
      if (!window.getSelection().getRangeAt(0).collapsed) {
        e.preventDefault()
        e.stopPropagation()

        return false
      }
    } catch (e) {
      console.log(e)
    }

    const {
      activeCalls,
      flashMessageAndReset,
      himages,
      updateHImagesState
    } = this.props

    let {
      selectedAll,
      himagesSelected
    } = this.props

    if (activeCalls.indexOf('callHImagesListEpic') > -1 || activeCalls.indexOf('actionHImagesEpic') > -1) {
      flashMessageAndReset('Wait for operation to be completed!', misc.messageType.WARNING)

      return
    }

    himagesSelected = selectedAll ? himages.slice() : himagesSelected.slice()
    const himageIndex = _.findIndex(himagesSelected, {id: himage.id})

    if (himageIndex < 0) {
      himagesSelected.push(himage)
    } else {
      himagesSelected.splice(himageIndex, 1)
      selectedAll = false
    }

    updateHImagesState({
      himagesSelected,
      selectedAll
    })
  }

  handleSelectAll() {
    this.props.updateHImagesState({
      himagesSelected: [],
      selectedAll: !this.props.selectedAll
    })
  }

  handleDeselectAll() {
    this.props.updateHImagesState({
      himagesSelected: [],
      selectedAll: false
    })
  }

  handleInterfaceChange() {
    clearTimeout(this.handleInterfaceChangeOffset)
    this.handleInterfaceChangeOffset = setTimeout(() => {
      this._recalcVisibleHImages()
    }, Math.round(this.props.himages.length / 5))
  }

  render() {
    const {accessToken, activeCalls, himages, himagesTotal, selectedAll, sScrollTop, sOffsetHeight, sScrollHeight} = this.props
    const himagesSelected = misc.empty(this.props.himagesSelected) ? [] : this.props.himagesSelected

    return ([
      (<Grid doubling key='himages-menu'>
        <Grid.Row>
          <Grid.Column width={16}>
            <Input pattern='' required title='no special chars, 3 minimum' fluid icon={<Icon name='search' link onClick={this.handleSearch}/>} placeholder='Search...' onChange={this.handleSearchedQuery} onKeyPress={this.handleSearchKeyPress} labelPosition='left' label={{content: `Total: ${himagesTotal}`}} value={this.state.searchedQuery}/>
          </Grid.Column>
        </Grid.Row>
        {
          misc.empty(accessToken) ? null :
            <Grid.Row>
              <Grid.Column width={16}>
                <Menu secondary>
                  <Menu.Menu position='left'>
                    {/* <Button color='blue' content={`${selectedAll ? 'Des' : 'S'}elect all`} label={{as: 'a', basic: true, content: himagesTotal}} labelPosition='right' onClick={this.handleSelectAll}/> */}
                    <Button color='blue' content='Deselect all' label={{as: 'a', basic: true, content: himagesSelected.length}} labelPosition='right' onClick={this.handleDeselectAll} disabled={himagesSelected.length < 1}/>
                  </Menu.Menu>
                  <Menu.Menu position='right'>
                    <ActionButtons key='action-buttons-himages' layout='horizontal' {...himagesSelected}/>
                  </Menu.Menu>
                </Menu>
              </Grid.Column>
            </Grid.Row>
        }
      </Grid>),
      (<div key='himages-list' className='himages-list' onLoad={this.handleInterfaceChange} onScroll={this.handleInterfaceChange} ref={wrapper => {this._wrapper = wrapper}}>
        <Grid doubling columns={6}>
          <Grid.Row>
            {himages.map((himage, hi) => <Grid.Column key={`himage-preview-${himage.id}`}>
              <HThumb himage={himage} hi={hi} selected={!misc.empty(_.find(himagesSelected, {id: himage.id})) || selectedAll} handleSelect={this.handleSelect} himagesLimit={himages.length} scrollableProps={{
                scrollTop: sScrollTop,
                offsetHeight: sOffsetHeight,
                scrollHeight: sScrollHeight
              }}/>
            </Grid.Column>)}
          </Grid.Row>
        </Grid>
      </div>),
      (<Grid key='himages-loader'>
        <Grid.Row>
          <Loader active={activeCalls.indexOf('callHImagesEpic') > -1}/>
        </Grid.Row>
      </Grid>)
    ])
  }
}

Home.propTypes = {
  accessToken: PropTypes.string,
  activeCalls: PropTypes.array,
  page: PropTypes.number,
  per_page: PropTypes.number,
  himages: PropTypes.array,
  himagesTotal: PropTypes.number,
  himagesSelected: PropTypes.array,
  selectedAll: PropTypes.bool,

  sOffsetHeight: PropTypes.number,
  sScrollHeight: PropTypes.number,
  sScrollTop: PropTypes.number
}

function mapStateToProps(state) {
  return {
    accessToken: state.workplace.accessToken,
    activeCalls: state.workplace.activeCalls,
    page: state.himages.page,
    per_page: state.himages.per_page,
    himages: state.himages.himages,
    himagesTotal: state.himages.himagesTotal,
    himagesSelected: state.himages.himagesSelected,
    selectedAll: state.himages.selectedAll,

    sOffsetHeight: state.himages.sOffsetHeight,
    sScrollHeight: state.himages.sScrollHeight,
    sScrollTop: state.himages.sScrollTop
  }
}

function mapDispatchToProps(dispatch) {
  return {
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch),
    callHImages: bindActionCreators(actions.callHImages, dispatch),
    updateHImagesState: bindActionCreators(actions.updateHImagesState, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
