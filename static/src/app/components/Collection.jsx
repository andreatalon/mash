import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Grid, Loader, Menu} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import ActionButtons from './ActionButtons'
import HThumb from './HThumb'
import _ from 'lodash'
import misc from '../misc/misc'

class Collection extends Component {
  constructor(props, context) {
    super(props, context)

    window.scrollTo(0, 0)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleSelectAll = this.handleSelectAll.bind(this)
    this.handleDeselectAll = this.handleDeselectAll.bind(this)
  }

  componentDidMount() {
    this.props.callMash(misc.ca())

    this.props.updateHImagesState({
      himagesSelected: [],
      selectedAll: false
    })
  }

  handleSelect(e, {himage}) {
    try {
      if (!window.getSelection().getRangeAt(0).collapsed) {
        e.preventDefault()
        e.stopPropagation()

        return false
      }
    } catch (e) {
      console.log(e)
    }

    const {
      activeCalls,
      flashMessageAndReset,
      updateHImagesState
    } = this.props

    if (activeCalls.indexOf('callMashEpic') > -1 || activeCalls.indexOf('actionHImagesEpic') > -1) {
      flashMessageAndReset('Wait for operation to be completed!', misc.messageType.WARNING)

      return
    }

    let {
      himagesSelected
    } = this.props

    himagesSelected = himagesSelected.slice()
    const mashIndex = _.findIndex(himagesSelected, {id: himage.id})

    if (mashIndex < 0) {
      himagesSelected.push(himage)
    } else {
      himagesSelected.splice(mashIndex, 1)
    }

    updateHImagesState({
      himagesSelected
    })
  }

  handleSelectAll() {
    this.props.updateHImagesState({
      himagesSelected: this.props.mash
    })
  }

  handleDeselectAll() {
    this.props.updateHImagesState({
      himagesSelected: []
    })
  }

  render() {
    const {accessToken, activeCalls, mash} = this.props
    const himagesSelected = misc.empty(this.props.himagesSelected) ? [] : this.props.himagesSelected

    return ([
      (<Grid doubling key='mash-menu'>
        {
          misc.empty(accessToken) ? null :
            <Grid.Row>
              <Grid.Column width={16}>
                <Menu secondary>
                  <Menu.Menu position='left'>
                    {/* <Button color='blue' content={`${mash.length === himagesSelected.length ? 'Des' : 'S'}elect all`} label={{as: 'a', basic: true, content: mash.length}} labelPosition='right' onClick={this.handleSelectAll}/> */}
                    <Button color='blue' content='Deselect all' label={{as: 'a', basic: true, content: himagesSelected.length}} labelPosition='right' onClick={this.handleDeselectAll} disabled={himagesSelected.length < 1}/>
                  </Menu.Menu>
                  <Menu.Menu position='right'>
                    <ActionButtons key='action-buttons-mash' layout='horizontal' {...himagesSelected}/>
                  </Menu.Menu>
                </Menu>
              </Grid.Column>
            </Grid.Row>
        }
      </Grid>),
      (<div key='mash-list' className='mash-list' onLoad={this.handleInterfaceChange} onScroll={this.handleInterfaceChange} ref={wrapper => {this._wrapper = wrapper}}>
        <Grid doubling columns={6} style={mash.length === 0 ? {display: 'none'} : {}}>
          <Grid.Row>
            {mash.map((himage, hi) => <Grid.Column key={`mash-preview-${himage.id}`}>
              <HThumb himage={himage} hi={hi} selected={!misc.empty(_.find(himagesSelected, {id: himage.id})) || mash.length === himagesSelected.length} handleSelect={this.handleSelect} himagesLimit={mash.length} scrollableProps={{
                scrollTop: null,
                offsetHeight: null,
                scrollHeight: null
              }}/>
            </Grid.Column>)}
          </Grid.Row>
        </Grid>
      </div>),
      (<Grid key='mash-loader'>
        <Grid.Row>
          <Loader active={activeCalls.indexOf('callMashEpic') > -1}/>
        </Grid.Row>
      </Grid>)
    ])
  }
}

Collection.propTypes = {
  accessToken: PropTypes.string,
  activeCalls: PropTypes.array,
  himagesSelected: PropTypes.array,
  mash: PropTypes.array
}

function mapStateToProps(state) {
  return {
    accessToken: state.workplace.accessToken,
    activeCalls: state.workplace.activeCalls,
    himagesSelected: state.himages.himagesSelected,
    mash: state.mash.mash
  }
}

function mapDispatchToProps(dispatch) {
  return {
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch),
    callMash: bindActionCreators(actions.callMash, dispatch),
    updateHImagesState: bindActionCreators(actions.updateHImagesState, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Collection)
