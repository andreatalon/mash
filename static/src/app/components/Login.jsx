import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Button} from 'semantic-ui-react'
import BlockingUI from './BlockingUI'

class Login extends Component {
  constructor(props, context) {
    super(props, context)

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault()
    this.props.parentProps.auth.authorize()
  }

  render() {
    return (
      <div className='login'>
        <BlockingUI/>
        <form onSubmit={this.handleSubmit}>
          <h1>Welcome</h1>
          <br/>
          <Button loading={this.props.activeCalls.length > 0} disabled={this.props.activeCalls.length > 0} primary type='submit'>Login</Button>
        </form>
      </div>
    )
  }
}

Login.propTypes = {
  activeCalls: PropTypes.array.isRequired
}

function mapStateToProps(state) {
  return {
    activeCalls: state.workplace.activeCalls
  }
}

export default connect(
  mapStateToProps
)(Login)
