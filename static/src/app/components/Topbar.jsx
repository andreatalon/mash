import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {Button, Input, Menu, Icon, Dropdown, Image} from 'semantic-ui-react'
import {actions} from '../actions/actions'
import Spinner from './Spinner'
import _ from 'lodash'
import history from '../misc/history'
import misc from '../misc/misc'

class Topbar extends Component {
  constructor(props, context) {
    super(props, context)

    this.handleGoto = this.handleGoto.bind(this)
    this.handleLogoutClick = this.handleLogoutClick.bind(this)
  }

  handleGoto(e, {gotolink}) {
    history.push(`${gotolink}`)
  }

  handleLogoutClick(e) {
    e.preventDefault()

    this.props.parentProps.auth.logout({federated: true})
  }

  render() {
    return (
      <Menu secondary className='topbar'>
        <Menu.Menu position='left'>
          <Menu.Item key='menuItemHome' gotolink='/' onClick={this.handleGoto}>
            <h1>Awesome app</h1>
          </Menu.Item>
          <Menu.Item key='menuItemLoader'>
            <Spinner/>
          </Menu.Item>
        </Menu.Menu>
        {
          misc.empty(this.props.accessToken) ? null :
            <Menu.Menu position='right'>
              <Menu.Item>
                <Button hidden={misc.empty(this.props.accessToken)} content='My collection' primary gotolink='/collection' onClick={this.handleGoto}/>
              </Menu.Item>
              {/* <Menu.Item>
                <Input icon={<Icon name='search' link onClick={this.handleSearch}/>} placeholder='Search...' onChange={this.handleSearchedQuery} onKeyPress={this.handleSearchKeyPress} value={this.state.searchedQuery}/>
              </Menu.Item> */}
              <Menu.Item name='Info Utente'>
                <Dropdown icon={<Icon size='big' name='user circle'/>} floating className='icon'>
                  <Dropdown.Menu>
                    <Dropdown.Header style={{textTransform: 'capitalize'}} content={`Welcome ${misc.ca().split('.')[0]}!`}/>
                    <Dropdown.Divider/>
                    <Dropdown.Item onClick={this.handleLogoutClick}>Logout</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </Menu.Item>
            </Menu.Menu>
        }
      </Menu>
    )
  }
}

Topbar.propTypes = {
  accessToken: PropTypes.string
}

function mapStateToProps(state) {
  return {
    accessToken: state.workplace.accessToken
  }
}

function mapDispatchToProps(dispatch) {
  return {
    flashMessageAndReset: bindActionCreators(actions.flashMessageAndReset, dispatch),
    waiting: bindActionCreators(actions.waiting, dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Topbar)
