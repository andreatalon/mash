import misc from '../misc/misc'

const types = {}, actions = {}, defs = {
  waiting: ['toggle', 'activeCalls', 'blockingCalls', 'silentCalls'],
  refresh: [],
  updateAccessToken: ['accessToken'],
  flashMessage: ['flashedMessage', 'messageType'],
  flashMessageAndReset: ['flashedMessage', 'messageType'],
  manageResponse: ['callResponse'],
  manageResponseAndReset: ['callResponse'],

  callHImages: ['keyword', 'page', 'per_page'],
  storeHImages: ['himages', 'page', 'per_page'],
  storeHImagesTotal: ['himagesTotal'],
  callHImage: ['id'],
  storeHImage: ['himage'],
  callMash: ['ref'],
  storeMash: ['mash'],
  callRecord: ['ref', 'himages', 'saving'],

  recordHImages: ['ref', 'himages', 'saving'],
  storeSearchedQuery: ['query'],
  updateHImagesState: ['state']
}

Object.keys(defs).map(def => {
  const params = defs[def].join(', ')
  const type = misc.camelToSnake(def)

  types[type] = String(type)
  actions[def] = misc.createFunction(def, params, `return {type:'${type}', ${params}}`)
})

export {
  types,
  actions,
  defs
}
