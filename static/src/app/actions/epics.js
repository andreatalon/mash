import {concat, empty, of} from 'rxjs'
import {ajax} from 'rxjs/ajax'
import {catchError, delay, filter, mergeMap, switchMap, timeout, tap} from 'rxjs/operators'
import {actions, types} from '../actions/actions'
import _ from 'lodash'
import config from '../misc/config'
import api from '../misc/api'
import misc from '../misc/misc'

export const flashMessageAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.FLASH_MESSAGE_AND_RESET), switchMap(action =>
    concat(
      of(actions.flashMessage(null)),
      of(actions.flashMessage(action.flashedMessage, action.messageType)),
      of(actions.flashMessage(null)).pipe(delay(6000))
    )))

export const manageResponseAndResetEpic = action$ =>
  action$.pipe(filter(action => action.type === types.MANAGE_RESPONSE_AND_RESET), switchMap(action =>
    concat(
      of(actions.manageResponse(action.callResponse)),
      of(actions.manageResponse(null))
    )))

export const callHImagesEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_HIMAGES), mergeMap(action =>
    concat(
      of(actions.waiting(true, ['callHImagesEpic'])),
      ajax({
        url: config.service('be', api.GET_SEARCH.replace(':keyword', action.keyword).replace(':page', action.page).replace(':per_page', action.per_page)),
        method: 'GET',
        headers: config.headers()
      })
        .pipe(
          mergeMap(response => concat(
            of(actions.storeHImagesTotal(response.response.total)),
            of(actions.storeHImages(response.response.results, action.page, action.per_page))
          )),
          catchError(error => of(actions.manageResponseAndReset(error)))
        ),
      of(actions.waiting(false, ['callHImagesEpic']))
    )))

export const callHImageEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_HIMAGE), mergeMap(action =>
    concat(
      of(actions.waiting(true, ['callHImageEpic'])),
      (!config.hasAuthorizedHeaders() ?
        empty() :
        ajax({
          url: config.service('be', api.GET_DETAIL.replace(':id', action.id)),
          method: 'GET',
          headers: config.authorizedHeaders()
        })
          .pipe(
            mergeMap(response => concat(
              of(actions.storeHImage(response.response)),
              of(actions.updateHImagesState({himagesSelected: [response.response], selectedAll: false}))
            )),
            catchError(error => of(actions.manageResponseAndReset(error)))
          )
      ),
      of(actions.waiting(false, ['callHImageEpic']))
    )))

export const callMashEpic = action$ =>
  action$.pipe(filter(action => action.type === types.CALL_MASH), mergeMap(action =>
    concat(
      of(actions.waiting(true, ['callMashEpic'])),
      (!config.hasAuthorizedHeaders() ?
        empty() :
        ajax({
          url: config.service('be', api.GET_MASH.replace(':ref', action.ref)),
          method: 'GET',
          headers: config.authorizedHeaders()
        })
          .pipe(
            mergeMap(response => of(actions.storeMash(response.response.himages))),
            catchError(error => of(actions.manageResponseAndReset(error)))
          )
      ),
      of(actions.waiting(false, ['callMashEpic']))
    )))

export const actionHImagesEpic = action$ =>
  action$.pipe(filter(action => [types.RECORD_HIMAGES].indexOf(action.type) > -1), mergeMap(action => {
    let delta = null
    let token = ''
    let body = {}

    switch (action.type) {
    case types.RECORD_HIMAGES: token = 'Record'; delta = action.saving ? 'Record' : 'Delete'; body = {ref: action.ref, himages: action.himages, saving: action.saving}; break
    default: return empty()
    }

    return concat(
      of(actions.waiting(true, ['actionHImagesEpic', `actionHImages${delta}Epic`])),
      (!config.hasAuthorizedHeaders() ?
        empty() :
        ajax({
          url: config.service('be', api[`POST_${token.toUpperCase()}`]),
          method: 'POST',
          headers: config.authorizedHeaders(),
          body
        })
          .pipe(
            mergeMap(response => concat(
              of(actions.manageResponseAndReset({code: 200, message: `${delta.toUpperCase()} action completed!`})).pipe(delay(3000)),
              of(actions.updateHImagesState({himagesSelected: []})),
              of(actions.callMash(action.ref))
            )),
            catchError(error => of(actions.manageResponseAndReset(error)))
          )
      ),
      of(actions.waiting(false, ['actionHImagesEpic', `actionHImages${delta}Epic`]))
    )
  }))
