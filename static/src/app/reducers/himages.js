import {types} from '../actions/actions'
import _ from 'lodash'
import misc from '../misc/misc'

const initialState = {
  himages: [],
  himagesTotal: 0,
  himagesSelected: [],
  page: 0,
  per_page: 100
}

function himages(state = initialState, action) {
  switch (action.type) {
  case types.STORE_HIMAGES:
    return Object.assign({}, state, {
      himages: _.uniqBy(misc.empty(action.page) ? action.himages : state.himages.concat(action.himages), 'id'),
      page: action.page
    })

  case types.STORE_HIMAGES_TOTAL:
    return Object.assign({}, state, {
      himagesTotal: action.himagesTotal
    })

  case types.STORE_HIMAGE: {
    const himages = state.himages.slice()

    if (!misc.empty(action.himage)) {
      const himageIndex = _.findIndex(himages, {id: action.himage.id})

      if (himageIndex > -1) {
        himages[himageIndex] = action.himage
      } else {
        himages.push(action.himage)
      }
    }

    return Object.assign({}, state, {
      himages
    })
  }

  case types.UPDATE_HIMAGES_STATE:
    return Object.assign({}, state, action.state)

  default:
    return state
  }
}

export default himages
