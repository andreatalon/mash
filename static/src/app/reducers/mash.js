import {types} from '../actions/actions'
import _ from 'lodash'

const initialState = {
  mash: []
}

function mash(state = initialState, action) {
  switch (action.type) {
  case types.STORE_MASH:
    return Object.assign({}, state, {
      mash: action.mash
    })

  default:
    return state
  }
}

export default mash
