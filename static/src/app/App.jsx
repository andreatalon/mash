import React from 'react'
import {Provider} from 'react-redux'
import {ConnectedRouter as Router} from 'connected-react-router'
import {Redirect, Switch} from 'react-router-dom'
import NoMatch from './components/NoMatch'
import Workplace from './components/Workplace'
import Home from './components/Home'
import Detail from './components/Detail'
import Collection from './components/Collection'
import Callback from './components/Callback'
import Login from './components/Login'
import Logout from './components/Logout'
import store from './misc/store'
import history from './misc/history'

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Redirect exact from='/' to='/home'/>
        <Workplace exact path='/home' component={Home}/>
        <Workplace exact path='/detail/:id' component={Detail}/>
        <Workplace exact path='/collection' component={Collection}/>
        <Workplace exact path='/callback' component={Callback}/>
        <Workplace exact path='/login' component={Login}/>
        <Workplace exact path='/logout' component={Logout}/>
        <Workplace path='*' component={NoMatch}/>
      </Switch>
    </Router>
  </Provider>
)

export default App
