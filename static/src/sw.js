self.addEventListener('install', e => {
  // const timeStamp = Date.now(); // ?ts=${timeStamp}
  e.waitUntil(
    caches
      .open('_manager_cache')
      .then(cache => cache
        .addAll([
          '/',
          '/?homescreen=1',
          '/index.html',
          '/index.html?homescreen=1',
          // '/css/index.css',
          // '/js/vendor.js',
          // '/js/index.js',
          // '/assets/images/_icon_thumb.png',
          // '/assets/images/_icon_thumb_white.png',
          '/manifest.json'
        ])
        .then(() => self.skipWaiting()))
      .catch(() => {// e
        // Do nothing; it's index.css in dev;
      })
  )
})

self.addEventListener('activate', e => {
  e.waitUntil(self.clients.claim())
})

self.addEventListener('fetch', e => {
  e.respondWith(
    caches.match(e.request, {ignoreSearch: true}).then(response => response || fetch(e.request))
  )
})
