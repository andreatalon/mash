const path = require('path')
const pkg = require('./package.json')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HappyPack = require('happypack')
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')

// const happyThreadPool = HappyPack.ThreadPool({size: 4});

module.exports = {
  mode: 'development',
  // devtool: 'source-map',
  entry: {
    vendor: Object.keys(pkg.dependencies),
    index: path.resolve(__dirname, 'src/index.js')
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'js/[name].js'
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true
        },
      }
    },
    runtimeChunk: false
  },
  devServer: {
    compress: true,
    disableHostCheck: true,
    inline: true,
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    historyApiFallback: true,
    host: '0.0.0.0',
    hot: true,
    https: false,
    noInfo: false,
    overlay: true,
    port: '8080',
    proxy: {
      '/sockjs-node': {
        target: '0.0.0.0:8080',
        ws: false
      }
    },
    progress: true,
    public: '0.0.0.0:8080',
    quiet: false,
    useLocalIp: true,
    watchOptions: {
      ignored: /node_modules/
    }
  },
  resolve: {
    alias: {
      '../../theme.config$': path.resolve(__dirname, 'semantic_theme/theme.config')
    },
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src')
    ],
    extensions: ['.js', '.jsx']
  },
  node: {
    Buffer: false,
    fs: 'empty'
  },
  externals: {
    winston: 'winston'
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        path.resolve('src/sw.js'),
        path.resolve('src/manifest.json'),
        {from: path.resolve(__dirname, 'node_modules/semantic-ui-less/themes/default/assets'), to: 'assets'},
        {from: path.resolve(__dirname, 'src/assets/favicons'), to: 'assets/favicons', noErrorOnMissing: true},
        {from: path.resolve(__dirname, 'src/assets/fonts'), to: 'assets/fonts', noErrorOnMissing: true},
        {from: path.resolve(__dirname, 'src/assets/images'), to: 'assets/images', noErrorOnMissing: true},
        {from: path.resolve(__dirname, 'src/assets/libs'), to: 'assets/libs', noErrorOnMissing: true}
      ]
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      indexCss: '',
      template: path.resolve(__dirname, 'src/index.ejs'),
      inject: false
    }),
    new webpack.HotModuleReplacementPlugin(),
    new HardSourceWebpackPlugin(),
    new HappyPack({
      id: 'js',
      loaders: ['babel-loader', {loader: 'eslint-loader', options: {include: path.resolve(__dirname, 'src'), enforce: 'pre', failOnError: true}}]
      // threadPool: happyThreadPool, debug: true, verbose: true
    }),
    new HappyPack({
      id: 'css',
      loaders: ['style-loader', 'css-loader']
      // threadPool: happyThreadPool, debug: true, verbose: true
    }),
    new HappyPack({
      id: 'less',
      loaders: ['style-loader', 'css-loader', 'less-loader'] // less-loader?sourceMap
      // threadPool: happyThreadPool, debug: true, verbose: true
    })
  ],
  module: {
    noParse: /lodash\/lodash.js/,
    rules: [
      {test: /\.(js|jsx)?$/, exclude: /node_modules/, use: 'happypack/loader?id=js'},
      {test: /\.html$/, use: [{loader: 'file-loader?name=[name].[ext]'}]},
      {test: /\.css$/, use: 'happypack/loader?id=css'},
      {test: /\.less$/, use: 'happypack/loader?id=less'},
      {test: /\.otf?$/, use: [{loader: 'url-loader?limit=10000&mimetype=application/octet-stream'}]},
      {test: /\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, use: [{loader: 'file-loader', options: {name: 'assets/fonts/[name].[ext]'}}]},
      {test: /\.(png|jpg|gif)$/, loader: 'file-loader'}
    ]
  }
}
