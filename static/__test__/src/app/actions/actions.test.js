'use strict'

import {actions, types} from '../../../../src/app/actions/actions'

describe('actions', () => {
  test('todo', () => {})

  test('waiting', () => {
    expect(actions.waiting).toBeDefined()

    const state = actions.waiting(true, null, false)
    expect(state.type).toEqual(types.WAITING)
  })

  test('refresh', () => {
    expect(actions.refresh).toBeDefined()

    const state = actions.refresh()
    expect(state.type).toEqual(types.REFRESH)
  })
})
